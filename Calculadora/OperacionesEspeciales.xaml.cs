﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Calculadora
{
    /// <summary>
    /// Lógica de interacción para OperacionesEspeciales.xaml
    /// </summary>
    public partial class OperacionesEspeciales : Window
    {
        int mathOperator = 0;
        public OperacionesEspeciales()
        {
            InitializeComponent();
        }

        private void CalculateButton_Click(object sender, RoutedEventArgs e)
        {
            decimal Num = 0;
            String result = "";
            bool c = false;
            try
            {
                c = decimal.TryParse(Operator1TextBox.Text, out Num);

                switch (mathOperator)
                {
                    case 0:
                        int cont = 0;
                        
                        for (int i = 1; i <= Num; i++)
                        {
                            if (Num % i == 0)
                            {
                                cont++;
                            }
                            if (cont != 2)
                            {
                                result = "No es primo";
                            }
                            else
                            {
                                result = "Es primo";
                            }
                        }

                        break;
                    case 1:
                        Decimal factorial = 1;
                        Decimal numA = Num;

                        while (Num != 0)
                        {
                            factorial *= Num;
                            Num--;
                        }
                        result = factorial.ToString();
                        break;
                    case 2:
                        Decimal a = 0, b = 1;
                        Decimal c1;
                        for (int j = 0; j < Num; j++)
                        {
                            c1 = a + b;
                            a = b;
                            b = c1;
                        }
                        result = a.ToString();
                        break;
                    case 3:
                        Decimal m = 1, k = 1, l;
                        for (int j = 0; j < Num; j++)
                        {                            
                            l = m + k;
                            m = k;
                            k = l;
                            result = m.ToString();
                        }
                        break;
                }
                ResultLabel.Content = result;
            }
            catch (Exception)
            {

            }
        }

        private void Checked_Handler(object sender, RoutedEventArgs e)
        {
            RadioButton rb = (RadioButton)sender;
            switch (rb.Name)
            {
                case "Prim":
                    mathOperator = 0;
                    break;
                case "Factorial":
                    mathOperator = 1;
                    break;
                case "nTermino":
                    mathOperator = 2;
                    break;
                case "desplegarSerie":
                    mathOperator = 3;
                    break;
            }
        }


        private void ClearButton_Click(object sender, RoutedEventArgs e)
        {
            ResultLabel.Content = "";
            Operator1TextBox.Text = "";
        }
    }
}
